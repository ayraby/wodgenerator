import React from "react";

import firebase from 'firebase';

import config from '../config.json';

export default class Database extends React.Component {
  constructor() {
    super(props);
  }

  firebaseConnect() {
    if (!firebase.apps.length) {
      firebase.initializeApp(config);
    }
    return firebase;
  }
}
