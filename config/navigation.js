import React from "react";
import { createBottomTabNavigator, TabBarBottom, createDrawerNavigator } from "react-navigation";
import { Icon } from 'native-base';

import HomeScreen from '../screens/HomeScreen';
import AchievementsScreen from '../screens/AchievementsScreen';
import PersonnalRecordsScreen from '../screens/PersonnalRecordsScreen';
import InfoScreen from '../screens/InfoScreen';

export default TabDrawerNavigator = createBottomTabNavigator({
  Home: { screen: HomeScreen },
  Achievements: { screen: AchievementsScreen },
  PersonnalRecords: { screen: PersonnalRecordsScreen },
  Info: { screen: InfoScreen }
}, {
  navigationOptions: ({ navigation }) => ({
    tabBarIcon: ({ focused, tintColor }) => {
      const { routeName } = navigation.state;
      let iconName;
      if (routeName === 'Home') {
        iconName = 'home';
      } else if (routeName === 'Achievements') {
        iconName = 'trophy';
      } else if (routeName === 'PersonnalRecords') {
        iconName = 'bonfire';
      } else if (routeName === 'Info') {
        iconName = 'information-circle';
      }
      return <Icon name={iconName} ios={`ios-${iconName}`} android={`md-${iconName}`} size={25} color={tintColor} />;
    },
  }),
  initialRouteName: 'Home',
  tabBarPosition: 'bottom',
  animationEnabled: false,
  swipeEnabled: false,
});
