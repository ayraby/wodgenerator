import React from 'react';
import { Image, Text, View } from 'react-native';
import { Card, CardItem, Right } from 'native-base';


export default class Wod extends React.Component {
  render() {
    const { content, duration, score, title } = this.props.exercise;
    const contentView = content.map((data, index) => {
      return (<Text key={`${index}`}>{`${data.num_movement} ${data.movement} ${data.weight}`}</Text>);
    })
    return(
      <Card style={{height: 'auto'}}>
        <CardItem header>
          <Image
            style={{flex:1, height: undefined, width: undefined}}
            resizeMode="contain"
            source={require("../assets/unsplash-1.jpg")} />
        </CardItem>
        <CardItem>
          <Text style={{fontSize: 22, fontWeight: 'bold'}}>{title}</Text>
          <Text style={{fontWeight: 'bold'}}>{score}</Text>
        </CardItem>
        <CardItem>
          <View>{contentView}</View>
          {duration !== "" && <Text>Cap Time: {duration} minutes</Text>}
        </CardItem>
      </Card>
    )
  }
}
