import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import TabDrawerNavigation from './config/navigation';

export default class App extends React.Component {
  render() {
    return (
      <TabDrawerNavigator />
    );
  }
}
