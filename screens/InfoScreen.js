import React from 'react';
import { Text } from 'react-native';
import { Body, Button,  Card, CardItem, Container, Content, Footer, FooterTab, Header, Left, Right, Icon, Title } from 'native-base';


class InfoScreen extends React.Component {
  render() {
    return (
      <Container>
        <Header>
          <Left />
          <Body>
            <Title>Info</Title>
          </Body>
          <Right />
        </Header>
        <Content>
          <Text>Info</Text>
        </Content>
      </Container>
    );
  }
}

export default InfoScreen;
