import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Body, Button,  Card, CardItem, Container, Content, Footer, FooterTab, H2, Header, Left, Right, Icon, Title } from 'native-base';

import Wod from '../components/Wod';

import Data from '../data.json';

import firebase from 'firebase';

import config from '../config.json';


class HomeScreen extends React.Component {
  state = {
    girls: [],
    wod: ""
  }
  constructor(props) {
    super(props);
  }

  /*
   * @TODO: Search why wod is sometimes undefined
   */
  _handlePress() {
    this.setState(previousState => {
      return {
        wod: this.state.girls[Math.floor(Math.random()*this.state.girls.length)] || ""
      };
    });
  }

  _handleCancelPress() {
    this.setState(previousState => {
      return { wod: "" };
    });
  }

  componentDidMount() {
    const girls = this.getData();
    let thisObj = this;
    // console.log(this.state);
    girls.on('value', function(snapshot){
      snapshotVal = snapshot.val();
      // console.log("SPASHOT VAL");
      this.setState(previousState => {
        return {
          girls: snapshot.val(),
          wod: ""
        }
      });
    }, this);
    console.log(this.state);
  }

  getData() {
    if (!firebase.apps.length) {
      firebase.initializeApp(config);
    }
    var girls = firebase.database().ref('girls');
    return girls;
  }

  render() {
    return (
      <Container>
        <Header style={styles.header}>
          <Left />
          <Body>
            <Title>Home</Title>
          </Body>
          <Right />
        </Header>
        <Content contentContainerStyle={styles.container}>
          <View style={styles.content}>
            <View style={styles.contentHeader}>
              <H2>
                WodGenerator
              </H2>
            </View>
            <View>
              {
                this.state.wod == "" &&
                <View>
                  <Text>Welcome to WodGenerator</Text>
                  <Text>Press the button to start</Text>
                </View>
              }
              {this.state.wod !== "" && <Wod exercise={this.state.wod}/>}
              {
                this.state.wod == "" &&
                <Button
                  info
                  rounded
                  style={styles.buttons}
                  onPress={() => this._handlePress()}>
                  <Text>Press Me!</Text>
                </Button>
              }
              {
                this.state.wod !== "" &&
                <Button
                  warning
                  rounded
                  style={styles.buttons}
                  onPress={() => this._handleCancelPress()}>
                  <Icon name="close-circle-outline" ios="ios-close-circle-outline" android="md-close-circle-outline" />
                  <Text>Cancel</Text>
                </Button>
              }
            </View>
          </View>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  header: { height: 90, borderBottomColor: "#757575" },
  container: { flex: 1, backgroundColor: '#fff', alignItems: 'center' },
  content: { flexGrow: 1, justifyContent: 'center' },
  contentHeader: {
    alignItems: "center",
    marginBottom: 50,
    backgroundColor: "transparent"
  },
  buttons: { alignSelf:"center", paddingLeft: 10, paddingRight: 10 },
});

export default HomeScreen;
