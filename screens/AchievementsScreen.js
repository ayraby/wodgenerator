import React from 'react';
import { Text, View } from 'react-native';
import {
  Body, Button,  Card, CardItem, Container, Content, Footer, FooterTab,
  Header, Left, Right, Icon, Title
} from 'native-base';

import {
  Form,
  Separator,
  InputField,
  LinkField,
  SwitchField,
  PickerField,
  DatePickerField,
  TimePickerField
} from 'react-native-form-generator';

const newMovement = { movement: "", num_movement: "", weight: "" };

class ContentInputs extends React.Component {
  onChange = name => value => {
    this.props.changeMethod({name: name, value: value});
  }

  render() {
    const { index, changeMethod } = this.props;
    return(
      <View>
        <InputField
          ref={`movement_${index}`}
          placeholder='Add movement'
          onChange={this.onChange(`movement_${index}`)}
        />
        <InputField
          ref={`num_movement_${index}`}
          placeholder='Add number movement'
          onChange={this.onChange(`num_movement_${index}`)}
        />
        <InputField
          ref={`weight_${index}`}
          placeholder='Add weight'
          onChange={this.onChange(`weight_${index}`)}
        />
        <Separator />
      </View>
    );
  }
}

class AchievementsScreen extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      formData: {
        title: "",
        duration: "",
        score_type: "FT",
        content: [
          {
            movement: "",
            num_movement: "",
            weight: "",
          }
        ]
      }
    }
  }

  // @TODO: Add validation
  addMovement() {
    const formData = {content: [...this.state.formData.content, newMovement]}
    this.handleFormChange(formData);
  }

  // @TODO: Add validation
  removeMovement() {
    const { content } = this.state.formData;
    const contentLength = content.length;
    if ( contentLength > 1 ) {
      const formData = {content: content.splice(contentLength-1, 1)}
      this.handleFormChange(formData);
    }
  }

  onCustomInputChange(obj){
    debugger;
    const { content } = this.state.formData;
    const contentLength = content.length;
    const name = obj.name.split('_')[0];
    const index = obj.name.split('_')[1];
    let value = obj.value;
    const formData = {...this.state.formData.content[index][name], value};
    // const formData = {content: [...this.state.formData.content, newMovement]};
    this.handleFormChange(formData);
  }

  handleFormChange(formData) {
    this.setState({formData:formData})
    this.props.onFormChange && this.props.onFormChange(formData);
  }

  render() {
    return (
      <Container>
        <Header>
          <Left />
          <Body>
            <Title>Achievements</Title>
          </Body>
          <Right />
        </Header>
        <Content>
          <Form
            ref='registrationForm'
            onChange={this.handleFormChange.bind(this)}
            label="Personal Information" >
            <Separator />
            <InputField ref='title' placeholder='Title'/>
            <Separator />
            <InputField ref='duration' placeholder='Duration'/>
            <PickerField ref='score_type'
              label='Score type'
              value='ft'
              options={{
                ft: 'FT',
                fl: 'FL',
                emom: 'EMOM',
                amrap: 'AMRAP'
              }}
            />
            <Separator />
            {
              this.state.formData.content &&
                this.state.formData.content.map((contentData, index) => {
                  return(
                    <ContentInputs
                      ref={`content_${index}`}
                      key={`content_${index}`}
                      index={index}
                      data={contentData}
                      changeMethod={this.onCustomInputChange.bind(this)}
                    />
                  );
                })
            }
            <Button
              info
              rounded
              onPress={() => this.addMovement()}>
              <Text>Add content</Text>
            </Button>
            <Button
              info
              rounded
              disabled={
                this.state.formData.content && this.state.formData.content.length <= 1
              }
              onPress={() => this.removeMovement()}>
              <Text>Remove content</Text>
            </Button>
          </Form>
          <Text>{JSON.stringify(this.state.formData)}</Text>
        </Content>
      </Container>
    );
  }
}

export default AchievementsScreen;
